import numpy as np

class Player:
    def __init__(self, token):
        self.token = token

    def __str__(self):
        return self.token
    
    def illegal(self):
        print("bad move ", self.token)
        return None
    
    def getMove(self, board):
        '''
        board will call this function to get a move 
        from the player whenever it is their turn
        '''
        print("this should be implemented")

    def startGame(self):
        pass

    def endGame(self, winner):
        pass
        
        
class HumanPlayer(Player):  
    '''
    HumanPlayer allows a human to play through the standard input
    '''
    def getMove(self, board):
        print("The board state is as follows:")
        print(board)
        print("your token is", self.token)
        print("\nWhat is your move (row col)?".format(board))
        raw = input()
        spltRaw = raw.split(" ")
        move = (int(spltRaw[0]), int(spltRaw[1]))
        print(move)
        return move
    
class RandomPlayer(Player):
    def getMove(self, board):
        moves = board.getLegalMovesIdx()
        return moves[  np.random.randint(len(moves))  ]
