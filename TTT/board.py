class Board:
    
    def __init__(self, p1, p2, 
                     state=[['-', '-', '-'],
                            ['-', '-', '-'],
                            ['-', '-', '-']]):
        
        self.state = state

        self.p1 = p1
        self.p2 = p2
        
    def __str__(self):
        return ("\n".join([" ".join(line) for line in self.state]))

    
    def isLegal(self, move):
        return (move[0] >= 0 and move[0] < 3 
                and move[1] >= 0 and move[1] < 3 
                and self.state[move[0]][move[1]] == '-')
            
    def apply(self, move, token):
        self.state[move[0]][move[1]] = token
        
        
    def draw(self):
        draw = True
        for r in self.state:
            for t in r:
                draw = draw and t!='-'
        # if draw:
        #     print("ITS A DRAW")
        return draw
        
        
    def winner(self):
        c00 = self.state[0][0] != '-'
        c11 = self.state[1][1] != '-'
        c22 = self.state[2][2] != '-'
        
        
        r0dc0 = (  #r0 d c0
                (self.state[0][0]==self.state[0][1] and self.state[0][1]==self.state[0][2]) or
                (self.state[0][0]==self.state[1][1] and self.state[1][1]==self.state[2][2]) or
                (self.state[0][0]==self.state[1][0] and self.state[1][0]==self.state[2][0]) 
               )
        r1c1sd = ( #r1 c1 sd
                (self.state[1][0]==self.state[1][1] and self.state[1][1]==self.state[1][2]) or
                (self.state[0][1]==self.state[1][1] and self.state[1][1]==self.state[2][1])   or
                (self.state[0][2]==self.state[1][1] and self.state[1][1]==self.state[2][0])
               )
        c2r2 = ( #c2 r2
                (self.state[0][2]==self.state[1][2] and self.state[1][2]==self.state[2][2]) or
                (self.state[2][0]==self.state[2][1] and self.state[2][1]==self.state[2][2])
               )
        win = (   (  c00 and r0dc0) or (c11 and r1c1sd) or (c22 and c2r2) )
                
        return win
    
    def getLegalMoves(self):
#         self.state == [['-', '-', '-'],
#                       ['-', '-', '-'],
#                       ['-', '-', '-']]
        moves = []
        for r, row in enumerate(self.state):
            for c, col in enumerate(self.state):
                if self.state[r][c] == '-':
                    moves.append((r, c))
        return moves  
    
    def getLegalMovesIdx(self):
#         self.state == [['-', '-', '-'],
#                       ['-', '-', '-'],
#                       ['-', '-', '-']]
        moves = []
        for r, row in enumerate(self.state):
            for c, col in enumerate(self.state):
                if self.state[r][c] == '-':
                    moves.append(r*3+c)
        return moves 
            

    def gameOver(self):
        return self.draw() or self.winner()
    