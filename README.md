# ML/RL for Games

Jack Langerman and Christian Baker

https://gitlab.com/JackLangerman/IntroMLProject.git


--> **[more recent work here](https://gitlab.com/JackLangerman/IntroMLProject/tree/neuralTree2)** <--  

Requirements:
- Python 3.x
- numpy
- ...

To Do:
1) Decide on environment
2) Make cool stuff!!

How to run locally:                       
1) 


Git Strategy (for contributors):

	Branching:
		Create a new branch for each feature
		1.
			git checkout -b [BranchName]		
			git checkout -b [WorkType_TaskName]		
			ex: 
				git checkout -b Feature_LogIn
				git checkout -b Bug_IncorrectHomepageStyleSheet

		2.
			git push origin [BranchName]
			ex:
				git push origin Feature_LogIn
		3.
			//do some work

		4.
			git add [fileName]

		5.
			git commit -m "[commit message PT# 000000]"

			ex:
				git commit -m "Implement policy gradients for Tic-Tac-Toe"
		6.
			git push

		7. merge TO master
			i. 
				go to gitlab.com/JackLangerman/IntroMLProject.....
				ex. 

			ii. 
				OBSERVE AND CHECK YOUR CHANGES!!!

			iii. 
				Click button to "Create New Pull Request"

			iv.
				...
			v.
				Once approval is recieved via Slack, click "Merge Pull Request" and delete branch if prudent. (don't forget to delete locally)





			
