import TTT.player
import TTT.board

class TicTacToe:
    def __init__(self, p1, p2, verbose=True):
        self.p1 = p1
        self.p2 = p2
        self.verbose = verbose
    
    def play(self):
        self.board = TTT.board.Board(self.p1, self.p2)
        if self.verbose:
            print("NEW GAME")
            print(self.board)
        
        self.p1.startGame()
        self.p2.startGame()

        cur = self.p1
        winner = None

        while True:
            move = cur.getMove(self.board)
            if self.board.isLegal(move):
                self.board.apply(move, cur.token)
#                 print(self.board)

                if self.board.winner():
                    if self.verbose:
                        print("And the winner is...\n...{}".format(cur.token))
                        print(cur)
                        print(self.board)
                    winner = cur
                    break
                    
                if self.board.draw():
                    if self.verbose:
                        print("Its a draw...")

                    winner = None
                    break
                
                if cur == self.p2:
                    cur = self.p1
                else:
                    cur = self.p2
            else:
                if self.verbose:
                    print("illegal")

                cur.illegal()
        
        if self.verbose:
            print(self.board)


        self.p1.endGame(winner)
        self.p2.endGame(winner)

        return winner

       